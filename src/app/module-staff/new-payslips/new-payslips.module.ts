import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NewPayslipsPageRoutingModule } from './new-payslips-routing.module';

import { NewPayslipsPage } from './new-payslips.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NewPayslipsPageRoutingModule
  ],
  declarations: [NewPayslipsPage]
})
export class NewPayslipsPageModule {}
