import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { Router, ActivatedRoute, NavigationExtras } from '@angular/router';
import { NavController } from '@ionic/angular';


@Component({
  selector: 'app-view-payslips',
  templateUrl: './view-payslips.page.html',
  styleUrls: ['./view-payslips.page.scss'],
})
export class ViewPayslipsPage implements OnInit {
  payslips: any;


  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private staffService: StaffService,
    private navCtrl: NavController
  ) {
    this.payslips = [];
    this.staffService.getAllPayslips().then((payslips) => {
      this.payslips = payslips;
    });
  }

  ngOnInit() {
  }

}
