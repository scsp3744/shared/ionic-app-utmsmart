import { Component, OnInit } from '@angular/core';
import { StaffService } from '../staff.service';
import { Observable } from 'rxjs';
import { NavigationExtras, Router } from '@angular/router';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-view-staff',
  templateUrl: './view-staff.page.html',
  styleUrls: ['./view-staff.page.scss'],
})
export class ViewStaffPage implements OnInit {
  results = null;

  constructor(
    private staffService: StaffService, 
    private router: Router,
    private alertCtrl: AlertController
  ) {
  }
  
  ngOnInit() {
    this.staffService.getStaffSimpleList().subscribe(
      results => {
        this.results = results;
      }
    );
  }

  ionViewDidEnter() {
    this.ngOnInit();
  }

  openUpdatePage(staff_id) {
    let navigationExtras: NavigationExtras = {
      state: {
        staff_id: staff_id
      }
    };
    this.router.navigate(['module-staff/update-staff'], navigationExtras);
  }

  openContractPage(staff_id) {
    let navigationExtras: NavigationExtras = {
      state: {
        staff_id: staff_id
      }
    };
    this.router.navigate(['module-staff/view-contract'], navigationExtras);
  }

  async presentAlertConfirm(staff_id) {
    const alert = await this.alertCtrl.create({
      header: 'Confirm!',
      message: 'Confirm removing this staff?',
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.staffService.removeSelectedStaff(staff_id).then(() => {
              this.ngOnInit();
            });
          }
        },
        {
          text: 'Cancel',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
          }
        }
      ]
    });

    await alert.present();
  }
}
