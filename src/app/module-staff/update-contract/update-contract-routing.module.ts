import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateContractPage } from './update-contract.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateContractPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateContractPageRoutingModule {}
