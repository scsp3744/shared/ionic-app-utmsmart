import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { StaffService } from '../staff.service';
import { NgForm } from '@angular/forms';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-add-contract',
  templateUrl: './add-contract.page.html',
  styleUrls: ['./add-contract.page.scss'],
})
export class AddContractPage implements OnInit {
  staff: any;
  staff_id: any;
  staff_contract: any;
  maxDate: any = (new Date()).getFullYear() + 100;
  today: any = new Date().toString().substr(0, 10);

  constructor(
    private route: ActivatedRoute, 
    private router: Router,
    private staffService: StaffService,
    private navCtrl: NavController
  ) { 
      this.staff = {};
      this.staff_contract = {};

      this.route.queryParams.subscribe(async params => {
        if (this.router.getCurrentNavigation().extras.state) {
          this.staff_id = this.router.getCurrentNavigation().extras.state.staff_id;
  
          this.staffService.getSelectedStaff(this.staff_id).then((staff) => {
            this.staff = staff;
          });
        }
      });
  }

  ngOnInit() {
  }

  submit(form: NgForm) {
    for (const [key, value] of Object.entries(form.value)) {
      if (value === '')
        return;

      this.staff_contract[key] = value;
    }
    this.staff_contract['staff_id'] = this.staff_id;
    this.save();
  }

  save(): void {
    if (this.staff_contract) {
      this.staffService.createStaffContract(this.staff_contract).then((data) => {
      })
      this.navCtrl.navigateForward('/module-staff/view-staff');
    }
  }

}
