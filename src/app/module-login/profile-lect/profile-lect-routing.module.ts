import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileLectPage } from './profile-lect.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileLectPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileLectPageRoutingModule {}
