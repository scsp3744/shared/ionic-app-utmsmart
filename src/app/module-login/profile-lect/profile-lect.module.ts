import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileLectPageRoutingModule } from './profile-lect-routing.module';

import { ProfileLectPage } from './profile-lect.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileLectPageRoutingModule
  ],
  declarations: [ProfileLectPage]
})
export class ProfileLectPageModule {}
