import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashStudPage } from './dash-stud.page';

describe('DashStudPage', () => {
  let component: DashStudPage;
  let fixture: ComponentFixture<DashStudPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashStudPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashStudPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
