import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashStudPageRoutingModule } from './dash-stud-routing.module';

import { DashStudPage } from './dash-stud.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashStudPageRoutingModule
  ],
  declarations: [DashStudPage]
})
export class DashStudPageModule {}
