import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashStudPage } from './dash-stud.page';

const routes: Routes = [
  {
    path: '',
    component: DashStudPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashStudPageRoutingModule {}
