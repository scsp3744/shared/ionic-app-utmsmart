import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashStafPageRoutingModule } from './dash-staf-routing.module';

import { DashStafPage } from './dash-staf.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashStafPageRoutingModule
  ],
  declarations: [DashStafPage]
})
export class DashStafPageModule {}
