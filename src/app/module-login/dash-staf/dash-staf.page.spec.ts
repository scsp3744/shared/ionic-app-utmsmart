import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashStafPage } from './dash-staf.page';

describe('DashStafPage', () => {
  let component: DashStafPage;
  let fixture: ComponentFixture<DashStafPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashStafPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashStafPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
