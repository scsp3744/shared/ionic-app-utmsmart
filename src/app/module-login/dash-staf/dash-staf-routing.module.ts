import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashStafPage } from './dash-staf.page';

const routes: Routes = [
  {
    path: '',
    component: DashStafPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class DashStafPageRoutingModule {}
