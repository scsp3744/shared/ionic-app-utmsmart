import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-forgot-pw',
  templateUrl: './forgot-pw.page.html',
  styleUrls: ['./forgot-pw.page.scss'],
})
export class ForgotPwPage implements OnInit {

  officialEmail:string;

  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
  }

  forgotPw(){
    var url = "http://localhost:5100/api/forgotPw";
    var body = {
      "officialEmail" : this.officialEmail
    };
    console.log(body);
    var data = this.http.post(url, body);
    data.subscribe(data => {
      if (data['message'])
        alert(data['message']);
    });
  }

}
