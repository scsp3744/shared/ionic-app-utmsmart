import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfileStudPageRoutingModule } from './profile-stud-routing.module';

import { ProfileStudPage } from './profile-stud.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfileStudPageRoutingModule
  ],
  declarations: [ProfileStudPage]
})
export class ProfileStudPageModule {}
