import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfileStudPage } from './profile-stud.page';

const routes: Routes = [
  {
    path: '',
    component: ProfileStudPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfileStudPageRoutingModule {}
