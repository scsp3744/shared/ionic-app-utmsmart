import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { DashLectPage } from './dash-lect.page';

describe('DashLectPage', () => {
  let component: DashLectPage;
  let fixture: ComponentFixture<DashLectPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DashLectPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(DashLectPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
