import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DashLectPageRoutingModule } from './dash-lect-routing.module';

import { DashLectPage } from './dash-lect.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DashLectPageRoutingModule
  ],
  declarations: [DashLectPage]
})
export class DashLectPageModule {}
