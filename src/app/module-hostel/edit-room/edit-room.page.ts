import { Component, OnInit } from '@angular/core';
import { RoomService } from '../room.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";
@Component({
  selector: 'app-edit-room',
  templateUrl: './edit-room.page.html',
  styleUrls: ['./edit-room.page.scss'],
})
export class EditRoomPage implements OnInit {
  updateRoomForm: FormGroup;
  id: any;
  constructor(private RoomService:RoomService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder) { this.id = this.actRoute.snapshot.paramMap.get('id');}

  ngOnInit() {
    this.getRoomData(this.id);
    this.updateRoomForm = this.fb.group({
      block: [''],
      roomno: [''],
      roomtype: [''],
      rentalrate: ['']
    })
  }

  getRoomData(id) {
    this.RoomService.getRoomId(id).subscribe(res => {
      this.updateRoomForm.setValue({
        block: res['block'],
        roomno: res['roomno'],
        roomtype: res['roomtype'],
        rentalrate: res['rentalrate']
      });
    });
  }

  updateForm() {
    if (!this.updateRoomForm.valid) {
      return false;
    } else {
      this.RoomService.updateRoom(this.id, this.updateRoomForm.value)
        .subscribe((res) => {
          console.log(res)
          this.updateRoomForm.reset();
          this.router.navigate(['../room']);
        })
    }
  }



}
