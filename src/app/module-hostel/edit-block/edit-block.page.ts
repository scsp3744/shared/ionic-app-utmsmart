import { Component, OnInit } from '@angular/core';
import { BlockService } from '../block.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-edit-block',
  templateUrl: './edit-block.page.html',
  styleUrls: ['./edit-block.page.scss'],
})
export class EditBlockPage implements OnInit {
  updateBlockForm: FormGroup;
  id: any;
  constructor(private BlockService: BlockService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder) { this.id = this.actRoute.snapshot.paramMap.get('id');}

  ngOnInit() {
    this.getBlockData(this.id);
    this.updateBlockForm = this.fb.group({
      college: [''],
      name: ['']
    })
  }

  getBlockData(id) {
    this.BlockService.getBlockId(id).subscribe(res => {
      this.updateBlockForm.setValue({
        college: res['college'],
        name: res['name']
      });
    });
  }

  updateForm() {
    if (!this.updateBlockForm.valid) {
      return false;
    } else {
      this.BlockService.updateBlock(this.id, this.updateBlockForm.value)
        .subscribe((res) => {
          console.log(res)
          this.updateBlockForm.reset();
          this.router.navigate(['../block']);
        })
    }
  }



}
