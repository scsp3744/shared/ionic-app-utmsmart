import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCollegePageRoutingModule } from './edit-college-routing.module';

import { EditCollegePage } from './edit-college.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    EditCollegePageRoutingModule
  ],
  declarations: [EditCollegePage]
})
export class EditCollegePageModule {}
