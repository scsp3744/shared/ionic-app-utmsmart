import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditCollegePage } from './edit-college.page';

describe('EditCollegePage', () => {
  let component: EditCollegePage;
  let fixture: ComponentFixture<EditCollegePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCollegePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditCollegePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
