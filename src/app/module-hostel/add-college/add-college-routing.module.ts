import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddCollegePage } from './add-college.page';

const routes: Routes = [
  {
    path: '',
    component: AddCollegePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddCollegePageRoutingModule {}
