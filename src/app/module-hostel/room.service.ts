import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RoomService {

  constructor(public http: HttpClient) { }

  getRoomId(id){
    return this.http.get('http://localhost:3000/room/' + id);
  }


  getRoom(){
    return this.http.get('http://localhost:3000/room');
  }

  getApplylist(){
    return this.http.get('http://localhost:3000/apply');
  }

  createRoom(room){
    console.log(room)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.post('http://localhost:3000/room', room, {headers: headers}).toPromise()
  }

  updateRoom(id, room){
    console.log(room)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put('http://localhost:3000/room/' + id, room, {headers: headers})
  }

  applyRoom(id, room){
    console.log(room)
    let headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');

    return this.http.put('http://localhost:3000/apply/' + id, room, {headers: headers})
  }

  deleteRoom(id){
    this.http.delete('http://localhost:3000/room/' + id).subscribe((res:any) =>
    {console.log(res);
  });
}
}
