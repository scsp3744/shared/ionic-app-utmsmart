import { Component, OnInit } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { RoomService } from '../room.service';
import { AddRoomPage } from '../add-room/add-room.page';

@Component({
  selector: 'app-applyhome',
  templateUrl: './applyhome.page.html',
  styleUrls: ['./applyhome.page.scss'],
})
export class ApplyhomePage implements OnInit {
  rooms: any;
  constructor(public nav: NavController, 
    public RoomService: RoomService, 
    public modalCtrl: ModalController) { 
 
    }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.rooms =[];
    this.RoomService.getApplylist().subscribe(rooms =>{
    this.rooms = rooms;
  })
  }
}
