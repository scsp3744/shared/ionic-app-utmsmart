import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ApplyhomePage } from './applyhome.page';

describe('ApplyhomePage', () => {
  let component: ApplyhomePage;
  let fixture: ComponentFixture<ApplyhomePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyhomePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ApplyhomePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
