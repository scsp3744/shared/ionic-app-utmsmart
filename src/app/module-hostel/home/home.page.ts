import { Component } from '@angular/core';
import { NavController, ModalController } from '@ionic/angular';
import { Router } from '@angular/router';
import { MenuController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  constructor(private route: Router, public menuController: MenuController) {}

  navigatetoHome(){
    this.route.navigate(['./home']);
  }

  navigatetoCollege(){
    this.route.navigate(['../college']);
  }

  navigatetoBlock(){
    this.route.navigate(['../block']);
  }

  navigatetoRoom(){
    this.route.navigate(['../room']);
  }
  
  navigatetoApply(){
    this.route.navigate(['../applyhome']);
  }


}
