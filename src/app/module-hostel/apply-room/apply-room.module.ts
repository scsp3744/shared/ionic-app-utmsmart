import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ApplyRoomPageRoutingModule } from './apply-room-routing.module';

import { ApplyRoomPage } from './apply-room.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ApplyRoomPageRoutingModule
  ],
  declarations: [ApplyRoomPage]
})
export class ApplyRoomPageModule {}
