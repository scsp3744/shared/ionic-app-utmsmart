import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddBlockPage } from './add-block.page';

const routes: Routes = [
  {
    path: '',
    component: AddBlockPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddBlockPageRoutingModule {}
