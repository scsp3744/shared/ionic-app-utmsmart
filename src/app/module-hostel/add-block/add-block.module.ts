import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddBlockPageRoutingModule } from './add-block-routing.module';

import { AddBlockPage } from './add-block.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddBlockPageRoutingModule
  ],
  declarations: [AddBlockPage]
})
export class AddBlockPageModule {}
