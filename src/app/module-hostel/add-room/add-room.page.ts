import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-add-room',
  templateUrl: './add-room.page.html',
  styleUrls: ['./add-room.page.scss'],
})
export class AddRoomPage implements OnInit {
  block: any;
  roomno: any;
  roomtype: any;
  rentalrate: any;
  student: any;

  constructor(public modalCtrl: ModalController) { }

  ngOnInit() {
  }
  AddRoom(form: NgForm){
    if(form.value.block === '' || form.value.roomno === '' || form.value.roomtype === '' || form.value.rentalrate ==='')
    {
      return;
    }
    this.block= form.value.block;
    this.roomno = form.value.roomno;
    this.roomtype = form.value.roomtype;
    this.rentalrate = form.value.rentalrate;
    this.save();
  }

  save():void {
    let room ={
      block: this.block,
      roomno: this.roomno,
      roomtype: this.roomtype,
      rentalrate: this.rentalrate,
      student : ""
    };
     this.modalCtrl.dismiss(room);
  }

  close():void{
    this.modalCtrl.dismiss();
  }
}
