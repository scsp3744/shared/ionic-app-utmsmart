import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-checkattendance',
  templateUrl: './checkattendance.page.html',
  styleUrls: ['./checkattendance.page.scss'],
})
export class CheckattendancePage implements OnInit {
  id: any;
  att: any;
  constructor(public navCtrl: NavController, public http: HttpClient) { }
  ngOnInit() {
    this.att = []
    this.id = '5f16c77748f72eddbd964dfe'
    this.http.get('http://localhost:5100/attendance/api/attendances/' + this.id).subscribe(data => {
      if (data['msg']) {
        alert(data['msg'])
        this.att = []
      }
      else {
        this.att = data['docs'];
      }
    })
  }
  submit() {

  }
}
