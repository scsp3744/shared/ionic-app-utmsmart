import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LecturerattendanceinPageRoutingModule } from './lecturerattendancein-routing.module';

import { LecturerattendanceinPage } from './lecturerattendancein.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LecturerattendanceinPageRoutingModule
  ],
  declarations: [LecturerattendanceinPage]
})
export class LecturerattendanceinPageModule {}
