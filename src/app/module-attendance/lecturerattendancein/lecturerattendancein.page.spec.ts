import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LecturerattendanceinPage } from './lecturerattendancein.page';

describe('LecturerattendanceinPage', () => {
  let component: LecturerattendanceinPage;
  let fixture: ComponentFixture<LecturerattendanceinPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecturerattendanceinPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LecturerattendanceinPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
