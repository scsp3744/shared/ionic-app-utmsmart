import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { NavController } from '@ionic/angular';
@Component({
  selector: 'app-lecturerattendancein',
  templateUrl: './lecturerattendancein.page.html',
  styleUrls: ['./lecturerattendancein.page.scss'],
})
export class LecturerattendanceinPage implements OnInit {

  ngOnInit() {
  }


  value: string = '';

  constructor(public http: HttpClient, private nav: NavController) { }

  submit() {
    if (this.value === '') return
    this.http.post('http://localhost:5100/attendance/api/attendances/in', { id_staff: this.value }).subscribe(data => {
      if (data['msg'])
        alert(data['msg']);
      else {
        console.log(data);
        alert("Checkin in for " + this.value + "\n" + "Date: " + new Date(data['attendance'].checkin_time).toDateString() + "\nTime: " + new Date(data['attendance'].checkin_time).toLocaleTimeString());
        this.nav.navigateRoot('/module-attendance/lecturerattendance');

      }
    })
  }

}
