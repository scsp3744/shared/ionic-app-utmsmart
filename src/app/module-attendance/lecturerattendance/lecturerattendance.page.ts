import { Component, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-lecturerattendance',
  templateUrl: './lecturerattendance.page.html',
  styleUrls: ['./lecturerattendance.page.scss'],
})
export class LecturerattendancePage implements OnInit {
  value: any
  constructor(public navCtrl: NavController, public http: HttpClient) {
    this.value = "5f16c77748f72eddbd964dfe"
  }

  ngOnInit() {
  }
  submit1() {
    if (this.value === '') return
    this.http.post('http://localhost:5100/attendance/api/attendances/in', { id_staff: this.value }).subscribe(data => {
      if (data['msg'])
        alert(data['msg']);
      else {
        console.log(data);
        alert("Checkin for " + localStorage.getItem('name') + "\n" + "Date: " + new Date(data['attendance'].checkin_time).toDateString() + "\nTime: " + new Date(data['attendance'].checkin_time).toLocaleTimeString());
        this.navCtrl.navigateRoot('/module-attendance/lecturerattendance');

      }
    })
  }
  submit2() {
    if (this.value === '') return
    this.http.post('http://localhost:5100/attendance/api/attendances/out', { id_staff: this.value }).subscribe(data => {
      if (data['msg'])
        alert(data['msg']);
      else {
        console.log(data);
        alert("Checkout for " + localStorage.getItem('name') + "\n" + "Date: " + new Date(data['attendance'].checkout_time).toDateString() + "\nTime: " + new Date(data['attendance'].checkout_time).toLocaleTimeString());

        this.navCtrl.navigateRoot('/module-attendance/lecturerattendance');

      }
    })
  }
}
