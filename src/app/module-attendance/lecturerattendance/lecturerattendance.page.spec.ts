import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LecturerattendancePage } from './lecturerattendance.page';

describe('LecturerattendancePage', () => {
  let component: LecturerattendancePage;
  let fixture: ComponentFixture<LecturerattendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecturerattendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LecturerattendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
