import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LecturerattendancePage } from './lecturerattendance.page';

const routes: Routes = [
  {
    path: '',
    component: LecturerattendancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LecturerattendancePageRoutingModule {}
