import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-add-request',
  templateUrl: './add-request.page.html',
  styleUrls: ['./add-request.page.scss'],
})
export class AddRequestPage implements OnInit {

  courses: any;
  course: any;
  sid: any;
  SelectedCourseID: any;
  lecturer: any;
  date: any;
  reason: any;
  file: any;

  constructor(public modalCtrl: ModalController, public http: HttpClient) { }

  ngOnInit() {
    this.sid = localStorage.getItem('name');
    this.http.get('http://localhost:5100/attendance/api/info/' + this.sid).subscribe(data => {
      this.courses = data;
      console.log(data)
    })
  }

  onChange() {
    var i;
    console.log(this.SelectedCourseID)
    for (i = 0; i < this.courses.length; i++) {
      if (this.courses[i].CurrentCourse._id == this.SelectedCourseID) {
        this.lecturer = this.courses[i].CurrentCourse.Lecturer.Name;
        this.course = this.courses[i].CurrentCourse.Name;
      }
    }
  }

  save(): void {

    let request = {
      reason: this.reason,
      SelectedCourseID: this.SelectedCourseID,
      date: new Date(this.date).valueOf(),
      file: this.file
    };

    this.modalCtrl.dismiss(request);
  }

  close(): void {
    this.modalCtrl.dismiss();
  }
}
