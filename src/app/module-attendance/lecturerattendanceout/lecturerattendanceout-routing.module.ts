import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LecturerattendanceoutPage } from './lecturerattendanceout.page';

const routes: Routes = [
  {
    path: '',
    component: LecturerattendanceoutPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LecturerattendanceoutPageRoutingModule {}
