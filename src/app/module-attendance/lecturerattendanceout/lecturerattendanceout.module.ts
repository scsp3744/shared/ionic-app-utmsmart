import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LecturerattendanceoutPageRoutingModule } from './lecturerattendanceout-routing.module';

import { LecturerattendanceoutPage } from './lecturerattendanceout.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LecturerattendanceoutPageRoutingModule
  ],
  declarations: [LecturerattendanceoutPage]
})
export class LecturerattendanceoutPageModule {}
