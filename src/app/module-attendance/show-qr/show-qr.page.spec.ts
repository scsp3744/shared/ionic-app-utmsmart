import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ShowQRPage } from './show-qr.page';

describe('ShowQRPage', () => {
  let component: ShowQRPage;
  let fixture: ComponentFixture<ShowQRPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShowQRPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ShowQRPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
