import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ShowQRPageRoutingModule } from './show-qr-routing.module';

import { ShowQRPage } from './show-qr.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ShowQRPageRoutingModule
  ],
  declarations: [ShowQRPage]
})
export class ShowQRPageModule {}
