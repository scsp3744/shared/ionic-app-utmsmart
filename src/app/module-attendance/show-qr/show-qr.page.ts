import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
// import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
// import QRCode from 'qrcode';

@Component({
  selector: 'app-show-qr',
  templateUrl: './show-qr.page.html',
  styleUrls: ['./show-qr.page.scss'],
})
export class ShowQRPage implements OnInit {
  code: any;
  generated = '';
  @Input() recent: any;
  encodeData: any;
  att: any;
  daurl: any;
  // constructor(public modalCtrl: ModalController, public http: HttpClient, private barcodeScanner: BarcodeScanner) {
  constructor(public modalCtrl: ModalController, public http: HttpClient) {
  }

  ngOnInit() {
    console.log(this.recent)
    this.code = this.recent.key;
    this.encodeData = "http://localhost:5100/attendance/api/attendance/scan/" + this.code;
    // const qrcode = QRCode;
    // qrcode.toDataURL(this.code, { errorCorrectionLevel: 'H' }, function (err, url) {
    //   this.generated = url;
    // })
    this.http.get('http://localhost:5100/attendance/api/attendancelist/' + this.recent._id).subscribe(data => {
      this.att = data;
    })
    this.daurl = "http://localhost:5100/attendance/attendanceScan/" + this.code;
  }
  dismiss() {
    // using the injected ModalController this page
    // can "dismiss" itself and optionally pass back data
    this.modalCtrl.dismiss({
      'dismissed': true
    });
  }
  encodedText() {
    // this.barcodeScanner
    //   .encode(this.barcodeScanner.Encode.TEXT_TYPE, this.encodeData)
    //   .then(
    //     encodedData => {
    //       console.log(encodedData);
    //       this.encodeData = encodedData;
    //     },
    //     err => {
    //       console.log("Error occured : " + err);
    //     }
    //   );
    window.open('http://localhost:5100/attendance/attendanceScan/' + this.code)
    // alert("Qr code only works in android mobile");
  }
}
