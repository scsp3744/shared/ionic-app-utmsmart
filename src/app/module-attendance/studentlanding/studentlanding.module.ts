import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StudentlandingPageRoutingModule } from './studentlanding-routing.module';

import { StudentlandingPage } from './studentlanding.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StudentlandingPageRoutingModule
  ],
  declarations: [StudentlandingPage]
})
export class StudentlandingPageModule {}
