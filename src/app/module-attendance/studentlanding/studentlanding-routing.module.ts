import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StudentlandingPage } from './studentlanding.page';

const routes: Routes = [
  {
    path: '',
    component: StudentlandingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StudentlandingPageRoutingModule {}
