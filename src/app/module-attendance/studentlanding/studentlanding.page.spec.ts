import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StudentlandingPage } from './studentlanding.page';

describe('StudentlandingPage', () => {
  let component: StudentlandingPage;
  let fixture: ComponentFixture<StudentlandingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StudentlandingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StudentlandingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
