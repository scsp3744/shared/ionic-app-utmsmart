import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { RegisterAttendancePage } from './register-attendance.page';

describe('RegisterAttendancePage', () => {
  let component: RegisterAttendancePage;
  let fixture: ComponentFixture<RegisterAttendancePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterAttendancePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(RegisterAttendancePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
