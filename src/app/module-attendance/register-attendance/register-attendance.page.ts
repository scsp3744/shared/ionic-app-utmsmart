import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from '@ionic/angular';
import { HttpClientModule, HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-register-attendance',
  templateUrl: './register-attendance.page.html',
  styleUrls: ['./register-attendance.page.scss'],
})
export class RegisterAttendancePage implements OnInit {
  username: string;
  password: string;
  constructor(public navCtrl: NavController, public http: HttpClient) { }

  ngOnInit() {
    this.username = localStorage.getItem('matricNo')
  }
  register() {
    if (!this.username || !this.password) {
      alert("please fill the form")
    }
    else {
      var url = "http://localhost:5100/attendance/api/register/attendance";
      var body = {
        matric: this.username,
        key: this.password
      };
      var data = this.http.post(url, body);
      data.subscribe(data => {
        if (data['attData']) {
          var from = new Date(data["attData"].timeStart).toLocaleString();
          var to = new Date(data["attData"].timeStop).toLocaleString().split(',')[1]
          var timeregister = new Date(data["attData"].TimeRegister).toLocaleString();

          alert(`Registered for 
${data["attData"].code} ${data["attData"].title}: ${data["attData"].lect}
${from} - ${to}

Name: ${data["std"]}
Time Registered: ${timeregister}
Status: ${data["result"].Attended}`)
          // redirect to another page on success
        }
        else if (data['msg']) {
          alert(data['msg'])
        }
      });
    }
  }
}
