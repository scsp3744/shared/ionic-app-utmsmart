import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ModalController } from '@ionic/angular';
import { ShowQRPage } from '../show-qr/show-qr.page';


@Component({
  selector: 'app-start-attendance',
  templateUrl: './start-attendance.page.html',
  styleUrls: ['./start-attendance.page.scss'],
})
export class StartAttendancePage implements OnInit {

  ngOnInit() {
  }



  courses: any;
  recentAttendances: any;
  SelectedCourse: any;
  constructor(public modalCtrl: ModalController, public http: HttpClient) { }
  ionViewDidEnter() {
    // this.reviewService.getReviews().then((data) => {
    //   console.log(data);
    //   this.reviews = data;
    // });
    // this.http.get('http://10.0.2.2:8080/api/lecturer/getCurrentCourse').subscribe(data => {
    this.http.get('http://localhost:5100/attendance/api/lecturer/getCurrentCourse').subscribe(data => {
      this.courses = data
      console.log(data)
    })
    // this.http.get('http://10.0.2.2:8080/api/lecturer/attendance').subscribe(data => {
    this.http.get('http://localhost:5100/attendance/api/lecturer/attendance').subscribe(data => {
      this.recentAttendances = data
      this.recentAttendances.reverse();
      console.log(data)
    })
    // this.requests = [{ Title: "abc" }, { Title: "haha" }]
  }

  async showModal(recent) {
    const modal = await this.modalCtrl.create({
      component: ShowQRPage,
      componentProps: {
        recent
      }
    });
    await modal.present();

    // process the data from modal as the modal's call dismiss func 
    // returned is a JSON object, eg { data: { title: 'abc', rating: 50 } }
    // therefore, use { data } (object deconstructing) to extract it
    // const { data } = await modal.onDidDismiss();

    // if (data) {
    //   var send = {
    //     sid: localStorage.getItem('sid'),
    //     cid: data.SelectedCourseID,
    //     reason: data.reason,
    //     requestOn: data.date
    //   }
    //   this.http.post('http://localhost:5100/attendance/api/requestLeave', send).subscribe(data => {
    //     if (data['message'] != 'Leave requested.')
    //       alert(data['message']);
    //     this.ionViewDidEnter();
    //   })
    //   console.log(data)
    // }
  }

  startAtt(courseID) {
    console.log(courseID)
    if (courseID === undefined) return;
    // this.http.post('http://10.0.2.2:8080/api/lecturer/startAttendance', { courseID }).subscribe(data => {
    this.http.post('http://localhost:5100/attendance/api/lecturer/startAttendance', { courseID }).subscribe(data => {
      console.log(data)
      this.recentAttendances.unshift(data)
    })
  }
  delete(courseID) {

    this.http.post('http://localhost:5100/attendance/api/lecturer/deleteAttendance', { courseID }).subscribe(data1 => {
      console.log(data1)
      this.http.get('http://localhost:5100/attendance/api/lecturer/attendance').subscribe(data => {
        this.recentAttendances = data
        this.recentAttendances.reverse();
        console.log(data)
      })
    })
  }
  getmsg() {
    // this.http.get('http://10.0.2.2:8080/api/msg').subscribe(data => {
    this.http.get('http://localhost:5100/attendance/api/msg').subscribe(data => {
      alert(data['msg']);
    })
  }

  message: string;
  sendmsg() {
    this.http.post('http://10.0.2.2:8080/api/form', { testing: this.message }).subscribe(data => {
      alert(data['msg']);
    })
  }
}
