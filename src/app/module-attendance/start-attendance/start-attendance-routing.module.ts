import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StartAttendancePage } from './start-attendance.page';

const routes: Routes = [
  {
    path: '',
    component: StartAttendancePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StartAttendancePageRoutingModule {}
