import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LecturerlandingPage } from './lecturerlanding.page';

describe('LecturerlandingPage', () => {
  let component: LecturerlandingPage;
  let fixture: ComponentFixture<LecturerlandingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LecturerlandingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LecturerlandingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
