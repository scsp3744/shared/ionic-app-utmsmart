import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LecturerlandingPage } from './lecturerlanding.page';

const routes: Routes = [
  {
    path: '',
    component: LecturerlandingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LecturerlandingPageRoutingModule {}
