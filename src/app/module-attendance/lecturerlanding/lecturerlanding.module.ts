import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LecturerlandingPageRoutingModule } from './lecturerlanding-routing.module';

import { LecturerlandingPage } from './lecturerlanding.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LecturerlandingPageRoutingModule
  ],
  declarations: [LecturerlandingPage]
})
export class LecturerlandingPageModule {}
