import { Component } from '@angular/core';
import { ResultService } from'../shared/result.service';
import { ModalController, AlertController } from '@ionic/angular';
import { NamePage } from '../name/name.page';

@Component({
  selector: 'app-academic-staff',
  templateUrl: './academic-staff.page.html',
  styleUrls: ['./academic-staff.page.scss'],
})


export class AcademicStaffPage {
  Sem:String
  students:any

  constructor(private alertCtrl:AlertController ,private result_Service: ResultService, private modalController: ModalController) {}

  async openModal() {
    const modal = await this.modalController.create({
      component: NamePage,
      componentProps: {
        students: this.students
      }
    });

    return await modal.present();// triggered when opening the modal
  }

  async failedAlert(res) {
    const alert = await this.alertCtrl.create({
      header: 'Failure',
      message: res,
      buttons: ['OK']
    });

    await alert.present();
  }

  getname(){
    console.log(this.Sem);
    this.result_Service.getname(this.Sem)
    .subscribe(
      res => {

        if(res=="NO RECORD" )
        {
          this.failedAlert(res)
        }
        else
        {
        console.log(res)
        this.students = res["student"]
        console.log(this.students)
        this.openModal();
        }
      },
      err => {
        console.log(err)
      }
    )
  }
}
