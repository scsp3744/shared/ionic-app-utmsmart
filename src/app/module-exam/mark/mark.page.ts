import { Component, OnInit, Input } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { PopoverController, NavController } from '@ionic/angular';
import { ResultService } from '../shared/result.service';

import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-mark',
  templateUrl: './mark.page.html',
  styleUrls: ['./mark.page.scss'],
})
export class MarkPage implements OnInit {

  @Input() public studentmark:any

  constructor(public result_service: ResultService,
    public popoverController: PopoverController,public navCtrl: NavController) { }

  ngOnInit() {
  }

  closePopover() {
    this.popoverController.dismiss();
  }

}
