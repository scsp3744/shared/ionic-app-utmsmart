import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EditCourseregPage } from './edit-coursereg.page';

describe('EditCourseregPage', () => {
  let component: EditCourseregPage;
  let fixture: ComponentFixture<EditCourseregPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditCourseregPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(EditCourseregPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
