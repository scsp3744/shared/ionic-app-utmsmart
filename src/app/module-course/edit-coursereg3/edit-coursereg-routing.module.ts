import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { EditCourseregPage } from './edit-coursereg.page';

const routes: Routes = [
  {
    path: '',
    component: EditCourseregPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class EditCourseregPageRoutingModule {}
