import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddCourseregPage } from './add-coursereg.page';

describe('AddCourseregPage', () => {
  let component: AddCourseregPage;
  let fixture: ComponentFixture<AddCourseregPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddCourseregPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddCourseregPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
