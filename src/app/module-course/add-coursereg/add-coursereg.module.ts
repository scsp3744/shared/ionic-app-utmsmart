import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IonicModule } from '@ionic/angular';

import { AddCourseregPageRoutingModule } from './add-coursereg-routing.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AddCourseregPage } from './add-coursereg.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AddCourseregPageRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [AddCourseregPage]
})
export class AddCourseregPageModule {}
