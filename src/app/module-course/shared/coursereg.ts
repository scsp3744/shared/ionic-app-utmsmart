export class Coursereg {
    _id: number;
    coursereg_name: string;
    artist: string;
    rating: string;
    status: string;
    course1: string;
    course2: string;
    course3: string;
    course4: string;
    course5: string;
    course6: string;
}