import { TestBed } from '@angular/core/testing';

import { CourseregService } from './coursereg.service';

describe('CourseregService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: CourseregService = TestBed.get(CourseregService);
    expect(service).toBeTruthy();
  });
});
