import { Injectable } from '@angular/core';
import { Coursereg } from './coursereg';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

export class CourseregService {

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  addCoursereg(coursereg: Coursereg): Observable<any> {
    return this.http.post<Coursereg>('http://localhost:3000/api/create-coursereg', coursereg, this.httpOptions)
      .pipe(
        catchError(this.handleError<Coursereg>('Add Coursereg'))
      );
  }

  getCoursereg(id): Observable<Coursereg[]> {
    return this.http.get<Coursereg[]>('http://localhost:3000/api/get-coursereg/' + id)
      .pipe(
        tap(_ => console.log(`Coursereg fetched: ${id}`)),
        catchError(this.handleError<Coursereg[]>(`Get Coursereg id=${id}`))
      );
  }

  getCourseregList(): Observable<Coursereg[]> {
    return this.http.get<Coursereg[]>('http://localhost:3000/api')
      .pipe(
        tap(courseregs => console.log('Courseregs fetched!')),
        catchError(this.handleError<Coursereg[]>('Get Courseregs', []))
      );
  }

  updateCoursereg(id, coursereg: Coursereg): Observable<any> {
    return this.http.put('http://localhost:3000/api/update-coursereg/' + id, coursereg, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Coursereg updated: ${id}`)),
        catchError(this.handleError<Coursereg[]>('Update Coursereg'))
      );
  }

  deleteCoursereg(id): Observable<Coursereg[]> {
    return this.http.delete<Coursereg[]>('http://localhost:3000/api/delete-coursereg/' + id, this.httpOptions)
      .pipe(
        tap(_ => console.log(`Coursereg deleted: ${id}`)),
        catchError(this.handleError<Coursereg[]>('Delete Coursereg'))
      );
  }


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      console.log(`${operation} failed: ${error.message}`);
      return of(result as T);
    }; }
}
