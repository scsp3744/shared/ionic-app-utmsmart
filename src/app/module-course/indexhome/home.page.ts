import { Component, OnInit } from '@angular/core';
import { CourseregService } from './../shared/coursereg.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  Courseregs: any = [];

  constructor(
    private courseregService: CourseregService
  ) {
  }

  ngOnInit() { }

  ionViewDidEnter() {
    this.courseregService.getCourseregList().subscribe((res) => {
      console.log(res)
      this.Courseregs = res;
    })
  }

  deleteCoursereg(coursereg, i) {
    if (window.confirm('Do you want to delete registration?')) {
      this.courseregService.deleteCoursereg(coursereg._id)
        .subscribe(() => {
          this.Courseregs.splice(i, 1);
          console.log('Registration deleted!')
        }
        )
    }
  }
}