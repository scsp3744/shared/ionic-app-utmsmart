import { Component, OnInit } from '@angular/core';
import { CourseregService } from '../shared/coursereg.service';
import { ActivatedRoute, Router } from "@angular/router";
import { FormGroup, FormBuilder } from "@angular/forms";

@Component({
  selector: 'app-edit-coursereg',
  templateUrl: './edit-coursereg.page.html',
  styleUrls: ['./edit-coursereg.page.scss'],
})
export class EditCourseregPage implements OnInit {

  updateCourseregForm: FormGroup;
  id: any;

  constructor(
    private courseregAPI: CourseregService,
    private actRoute: ActivatedRoute,
    private router: Router,
    public fb: FormBuilder
  ) {
    this.id = this.actRoute.snapshot.paramMap.get('id');
  }

  ngOnInit() {
    this.getCourseregData(this.id);
    this.updateCourseregForm = this.fb.group({
      course1: [''],
      course2: [''],
      course3: [''],
      course4: ['']
    })
  }

  getCourseregData(id) {
    this.courseregAPI.getCoursereg(id).subscribe(res => {
      this.updateCourseregForm.setValue({
        course1: res['course1'],
        course2: res['course2'],
        course3: res['course3'],
        course4: res['course4']

      });
    });
  }

  updateForm() {
    if (!this.updateCourseregForm.valid) {
      return false;
    } else {
      this.courseregAPI.updateCoursereg(this.id, this.updateCourseregForm.value)
        .subscribe((res) => {
          console.log(res)
          this.updateCourseregForm.reset();
          this.router.navigate(['/home']);
        })
    }
  }

}
