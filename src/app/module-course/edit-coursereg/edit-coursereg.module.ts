import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { EditCourseregPageRoutingModule } from './edit-coursereg-routing.module';

import { EditCourseregPage } from './edit-coursereg.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    EditCourseregPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [EditCourseregPage]
})
export class EditCourseregPageModule {}
