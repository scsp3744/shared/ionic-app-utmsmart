import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'landing',
    pathMatch: 'full'
  },
  // module course reg
  { path: 'module-course', redirectTo: 'module-course/index', pathMatch: 'full' },
  { path: 'module-course/index', loadChildren: () => import('./module-course/indexhome/home.module').then(m => m.HomePageModule) },
  {
    path: 'module-course/home',
    loadChildren: () => import('./module-course/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-course/home2',
    loadChildren: () => import('./module-course/home2/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-course/home3',
    loadChildren: () => import('./module-course/home3/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-course/home4',
    loadChildren: () => import('./module-course/home4/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-course/add-course',
    loadChildren: () => import('./module-course/add-coursereg/add-coursereg.module').then(m => m.AddCourseregPageModule)
  },
  {
    path: 'module-course/modify-course/:id',
    loadChildren: () => import('./module-course/edit-coursereg/edit-coursereg.module').then(m => m.EditCourseregPageModule)
  },
  {
    path: 'module-course/modify-course2/:id',
    loadChildren: () => import('./module-course/edit-coursereg2/edit-coursereg.module').then(m => m.EditCourseregPageModule)
  },
  {
    path: 'module-course/modify-course3/:id',
    loadChildren: () => import('./module-course/edit-coursereg3/edit-coursereg.module').then(m => m.EditCourseregPageModule)
  },
  //module hostel
  {
    path: 'module-hostel/home',
    loadChildren: () => import('./module-hostel/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-hostel',
    redirectTo: 'module-hostel/home',
    pathMatch: 'full'
  },
  {
    path: 'module-hostel/college',
    loadChildren: () => import('./module-hostel/college/college.module').then(m => m.CollegePageModule)
  },
  {
    path: 'module-hostel/block',
    loadChildren: () => import('./module-hostel/block/block.module').then(m => m.BlockPageModule)
  },
  {
    path: 'module-hostel/room',
    loadChildren: () => import('./module-hostel/room/room.module').then(m => m.RoomPageModule)
  },
  {
    path: 'module-hostel/add-college',
    loadChildren: () => import('./module-hostel/add-college/add-college.module').then(m => m.AddCollegePageModule)
  },
  {
    path: 'module-hostel/add-block',
    loadChildren: () => import('./module-hostel/add-block/add-block.module').then(m => m.AddBlockPageModule)
  },
  {
    path: 'module-hostel/add-room',
    loadChildren: () => import('./module-hostel/add-room/add-room.module').then(m => m.AddRoomPageModule)
  },
  {
    path: 'module-hostel/edit-college/:id',
    loadChildren: () => import('./module-hostel/edit-college/edit-college.module').then(m => m.EditCollegePageModule)
  },
  {
    path: 'module-hostel/edit-block/:id',
    loadChildren: () => import('./module-hostel/edit-block/edit-block.module').then(m => m.EditBlockPageModule)
  },
  {
    path: 'module-hostel/edit-room/:id',
    loadChildren: () => import('./module-hostel/edit-room/edit-room.module').then(m => m.EditRoomPageModule)
  },
  {
    path: 'module-hostel/applyhome',
    loadChildren: () => import('./module-hostel/applyhome/applyhome.module').then(m => m.ApplyhomePageModule)
  },
  {
    path: 'module-hostel/apply-room/:id',
    loadChildren: () => import('./module-hostel/apply-room/apply-room.module').then(m => m.ApplyRoomPageModule)
  },
  // module exam
  {
    path: 'module-exam/home',
    loadChildren: () => import('./module-exam/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-exam',
    redirectTo: 'module-exam/home',
    pathMatch: 'full'
  },
  {
    path: 'module-exam/result-slip',
    loadChildren: () => import('./module-exam/result-slip/result-slip.module').then(m => m.ResultSlipPageModule)
  },
  {
    path: 'module-exam/resultpage/:id',
    loadChildren: () => import('./module-exam/result-page/result-page.module').then(m => m.ResultPagePageModule)
  },
  {
    path: 'module-exam/student-list',
    loadChildren: () => import('./module-exam/student-list/student-list.module').then(m => m.StudentListPageModule)
  },
  {
    path: 'module-exam/update-result',
    loadChildren: () => import('./module-exam/update-result/update-result.module').then(m => m.UpdateResultPageModule)
  },
  {
    path: 'module-exam/lecturer',
    loadChildren: () => import('./module-exam/lecturer/lecturer.module').then(m => m.LecturerPageModule)
  },
  {
    path: 'module-exam/academic-staff',
    loadChildren: () => import('./module-exam/academic-staff/academic-staff.module').then(m => m.AcademicStaffPageModule)
  },
  // module student admission
  {
    path: 'module-student-admission/home',
    loadChildren: () => import('./module-student-admission/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-student-admission/message/:id',
    loadChildren: () => import('./module-student-admission/view-message/view-message.module').then(m => m.ViewMessagePageModule)
  },
  {
    path: 'module-student-admission',
    redirectTo: 'module-student-admission/home',
    pathMatch: 'full'
  },
  // module staff
  {
    path: 'module-staff',
    redirectTo: 'module-staff/home',
    pathMatch: 'full'
  },
  {
    path: 'module-staff/home',
    loadChildren: () => import('./module-staff/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-staff/view-staff',
    loadChildren: () => import('./module-staff/view-staff/view-staff.module').then(m => m.ViewStaffPageModule)
  },
  {
    path: 'module-staff/add-staff',
    loadChildren: () => import('./module-staff/add-staff/add-staff.module').then(m => m.AddStaffPageModule)
  },
  {
    path: 'module-staff/update-staff',
    loadChildren: () => import('./module-staff/update-staff/update-staff.module').then(m => m.UpdateStaffPageModule)
  },
  {
    path: 'module-staff/view-contract',
    loadChildren: () => import('./module-staff/view-contract/view-contract.module').then(m => m.ViewContractPageModule)
  },
  {
    path: 'module-staff/update-contract',
    loadChildren: () => import('./module-staff/update-contract/update-contract.module').then(m => m.UpdateContractPageModule)
  },
  {
    path: 'module-staff/add-contract',
    loadChildren: () => import('./module-staff/add-contract/add-contract.module').then(m => m.AddContractPageModule)
  },
  {
    path: 'module-staff/new-payslips',
    loadChildren: () => import('./module-staff/new-payslips/new-payslips.module').then(m => m.NewPayslipsPageModule)
  },
  {
    path: 'module-staff/view-payslips',
    loadChildren: () => import('./module-staff/view-payslips/view-payslips.module').then(m => m.ViewPayslipsPageModule)
  },
  // module attendance
  {
    path: 'module-attendance/home',
    loadChildren: () => import('./module-attendance/home/home.module').then(m => m.HomePageModule)
  },
  {
    path: 'module-attendance',
    redirectTo: 'module-attendance/home',
    pathMatch: 'full'
  },
  {
    path: 'module-attendance/show-qr',
    loadChildren: () => import('./module-attendance/show-qr/show-qr.module').then(m => m.ShowQRPageModule)
  },
  {
    path: 'module-attendance/register-attendance',
    loadChildren: () => import('./module-attendance/register-attendance/register-attendance.module').then(m => m.RegisterAttendancePageModule)
  },
  {
    path: 'module-attendance/student',
    loadChildren: () => import('./module-attendance/student/student.module').then(m => m.StudentPageModule)
  },
  {
    path: 'module-attendance/add-request',
    loadChildren: () => import('./module-attendance/add-request/add-request.module').then(m => m.AddRequestPageModule)
  },
  {
    path: 'module-attendance/start-attendance',
    loadChildren: () => import('./module-attendance/start-attendance/start-attendance.module').then(m => m.StartAttendancePageModule)
  },
  {
    path: 'module-attendance/checkattendance',
    loadChildren: () => import('./module-attendance/checkattendance/checkattendance.module').then(m => m.CheckattendancePageModule)
  },
  {
    path: 'module-attendance/lecturerattendance',
    loadChildren: () => import('./module-attendance/lecturerattendance/lecturerattendance.module').then(m => m.LecturerattendancePageModule)
  },
  {
    path: 'module-attendance/lecturerattendancein',
    loadChildren: () => import('./module-attendance/lecturerattendancein/lecturerattendancein.module').then(m => m.LecturerattendanceinPageModule)
  },
  {
    path: 'module-attendance/lecturerattendanceout',
    loadChildren: () => import('./module-attendance/lecturerattendanceout/lecturerattendanceout.module').then(m => m.LecturerattendanceoutPageModule)
  },
  {
    path: 'module-attendance/studentlanding',
    loadChildren: () => import('./module-attendance/studentlanding/studentlanding.module').then(m => m.StudentlandingPageModule)
  },
  {
    path: 'module-attendance/lecturerlanding',
    loadChildren: () => import('./module-attendance/lecturerlanding/lecturerlanding.module').then(m => m.LecturerlandingPageModule)
  },
  //module login
  {
    path: 'module-login/home',
    loadChildren: () => import('./module-login/home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'module-login',
    redirectTo: 'module-login/home',
    pathMatch: 'full'
  },
  {
    path: 'module-login/change-pw',
    loadChildren: () => import('./module-login/change-pw/change-pw.module').then( m => m.ChangePwPageModule)
  },
  {
    path: 'module-login/dash-lect',
    loadChildren: () => import('./module-login/dash-lect/dash-lect.module').then( m => m.DashLectPageModule)
  },
  {
    path: 'module-login/dash-staf',
    loadChildren: () => import('./module-login/dash-staf/dash-staf.module').then( m => m.DashStafPageModule)
  },
  {
    path: 'module-login/dash-stud',
    loadChildren: () => import('./module-login/dash-stud/dash-stud.module').then( m => m.DashStudPageModule)
  },
  {
    path: 'module-login/forgot-pw',
    loadChildren: () => import('./module-login/forgot-pw/forgot-pw.module').then( m => m.ForgotPwPageModule)
  },
  {
    path: 'module-login/profile-lect',
    loadChildren: () => import('./module-login/profile-lect/profile-lect.module').then( m => m.ProfileLectPageModule)
  },
  {
    path: 'module-login/profile-staf',
    loadChildren: () => import('./module-login/profile-staf/profile-staf.module').then( m => m.ProfileStafPageModule)
  },
  {
    path: 'module-login/profile-stud',
    loadChildren: () => import('./module-login/profile-stud/profile-stud.module').then( m => m.ProfileStudPageModule)
  },
  //
  {
    path: 'landing',
    loadChildren: () => import('./landing/landing.module').then(m => m.LandingPageModule)
  },

];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
